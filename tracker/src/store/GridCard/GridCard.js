import React from 'react';
import PropTypes from 'prop-types';
import './GridCard.css';

function GridCard({ data }) {
	return (
		<div className='main'>
          
            <img className='image' src={data?.oImg.sUrl} />
            
			

			<div className='t'>
				<a href='#'>{data?.sTitle}</a>
			</div>
			
		</div>
	)
}

GridCard.propTypes = {
	data: PropTypes.object,
}

export default GridCard
