import React from 'react';
import PropTypes from 'prop-types';
import './SmallCard.css';

function SmallCard({ data }) {
	return (
		<div className='sc'>
			
			<img className='img' src={data?.oImg.sUrl} />

			<div className='title'>
				<t>{data?.sTitle}</t>
			</div>
			
		</div>
	)
}

SmallCard.propTypes = {
	data: PropTypes.object,
}
export default SmallCard
