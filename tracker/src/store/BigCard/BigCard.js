import React from 'react';
import PropTypes from 'prop-types';
import './BigCard.css';

function BigCard({ data }) {

	return (
		<div className='box'>

			<img className='img1' src="https://www.crictracker.com/wp-content/uploads/2018/05/Chennai-Super-Kings-team.jpg" />
			
			<div className='title'>
				<a>{data?.sTitle}</a>
				<p className='desc'>{data?.sDescription}</p>
			</div>
			
		</div>
	)
}

BigCard.propTypes = {
	data: PropTypes.object,
}

export default BigCard;
