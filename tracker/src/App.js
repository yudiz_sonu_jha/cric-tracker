import React, { lazy, useEffect, useState } from 'react';
import { graphQlToRest } from './data';
import './App.css';

const SmallCard = lazy(() => import('./store/SmallCard/SmallCard'));
const BigCard = lazy(() => import('./store/BigCard/BigCard'));
const GridCard = lazy(() => import('./store/GridCard/GridCard'));

function App() {
	const [ data, setData ] = useState();

	useEffect(() => {
		graphQlToRest().then((items) =>
			setData(items.data.getHomePageArticle.aResults)
		)
	}, [])

	return (
<>	
		<div className='App'>
			{data?.map(( items,i) => {
				return (
					<React.Fragment key={i}>
						{items.aArticle
							.filter((bigCard) => bigCard.sType === 'nBig')
							.map((bigCard) => (
								<BigCard key={bigCard._id} data={bigCard} />
							))}

						{items.aArticle
							.filter((smallCard) => smallCard.sType === 'nSmall')
							.map((smallCard) => (
								<SmallCard key={smallCard._id} data={smallCard} />
							))}

						<div className='container'>
							{items.aArticle
								.filter((gridCard) => gridCard.sType === 'nGrid')
								.map((gridCard) => (
									<GridCard key={gridCard._id} data={gridCard} />
								))}
						</div>
					</React.Fragment>
				)
			})}
		</div>
</>
	)
}

export default App;
